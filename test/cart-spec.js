'use strict';

import chai from 'chai';
chai.should();
import Cart from '../src/business/cart';
import {products} from '../src/store/products';

describe('Cart', function(){

  let cart;

  beforeEach(function(){
    cart = new Cart(products);
  });

  it('should set "euro" as default currency', function(done){
    cart._currency.name.should.equal('euro');
    done();
  });

  it('should throw error if not registered currency', function(done){
    (function(){
      cart = new Cart(products, 'dolar');
    }).should.throw(Error);

    done();
  });

  it('should throw error if not products are defined', function(done){
    (function(){
      cart = new Cart();
    }).should.throw(Error);

    done();
  });

  it('adds quantity when adding same product multiple times', function(done){
    cart.addItem('VOUCHER');
    cart.addItem('VOUCHER');
    cart.items.VOUCHER.quantity.should.equal(2);
    done();
  });

  it('should throw error when trying to add not registered item type', function(done){
    (function(){
      cart.addItem('KANGAROO');
    }).should.throw(Error);
    done();
  });

});