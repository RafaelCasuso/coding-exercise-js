'use strict';

import chai from 'chai';
chai.should();
import Checkout from '../src/business/checkOut';
import {products} from '../src/store/products';
import {discountExamples} from '../src/store/discounts';

const wrongDiscounts = {
  'N_FOR_P': {
    initialNumber: 2,
    finalNumber: 1,
    itemType: 'VOUCHER'
  },
  'BULK_PURCHASY': {
    minNumber: 3,
    itemPrice: 19,
    itemType: 'TSHIRT'
  }
};

describe('Checkout', function(){

  let co;

  beforeEach(function(){
    co = new Checkout({products, discounts: discountExamples});
  });

  it('should be initialized with empty cart', function(done){
    co.cart.items.should.be.empty;
    done();
  });

  it('should accept pricing rules', function(done){
    co._discounts.should.exist;
    done();
  });

  it('should throw error if no products are provided', function (done) {
    (function(){
      co = new Checkout({discounts: wrongDiscounts});
    }).should.throw(Error);

    done();
  });

  it('should throw error with pricing rules including discount not defined in discounts', function (done) {
    (function(){
      co = new Checkout({products, discounts: wrongDiscounts});
    }).should.throw(Error);

    done();
  });

  it('should scan a product', function(done){
    co.scan('VOUCHER');
    done();
  });

  it('should scan in chainable way', function(done){
    co.scan('VOUCHER').scan('TSHIRT');
    done();
  });

  it('should scan multiple products at once', function(done){
    co.multiScan(['VOUCHER', 'MUG']);
    done();
  });

  it('should show total results when finished', function(done){
    co.scan('VOUCHER').scan('TSHIRT');
    co.total();
    done();
  });

});