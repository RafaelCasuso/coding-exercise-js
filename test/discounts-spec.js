'use strict';

import chai from 'chai';
chai.should();
import {discounts} from '../src/business/discounts';

const pricingRules = {
  'N_FOR_N': {
    initialNumber: 2,
    finalNumber: 1,
    itemType: 'VOUCHER'
  },
  'BULK_PURCHASE': {
    minNumber: 3,
    itemPrice: 19,
    itemType: 'TSHIRT'
  }
};
const wrongPricingRules = {
  'N_FOR_N': {
    initialNumber: '2',
    finalNumber: 1,
    itemType: 'VOUCHER'
  },
  'BULK_PURCHASE': {
    minNumber: 3,
    itemPrice: '19',
    itemType: 'TSHIRT'
  }
};

describe('Discounts', function () {

  let fakeItems;

  beforeEach(function () {
    fakeItems = {
      'VOUCHER': {
        quantity: 2,
        price: 10
      },
      'TSHIRT': {
        quantity: 3,
        price: 20
      }
    };
  });

  it('N_FOR_N should apply discount when min number of products', function (done) {

    discounts.N_FOR_N(fakeItems, pricingRules.N_FOR_N);
    fakeItems.VOUCHER.price.should.equal(5);
    done();
  });

  it('N_PURCHASE should apply discount when min number of products', function (done) {

    discounts.BULK_PURCHASE(fakeItems, pricingRules.BULK_PURCHASE);
    fakeItems.TSHIRT.price.should.equal(19);
    done();
  });

});