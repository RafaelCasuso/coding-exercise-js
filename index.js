'use strict';

import Checkout from './src/business/checkOut';
import {products} from './src/store/products';
import {discountExamples} from './src/store/discounts';

//Exercise required interface
let co = new Checkout({products, discounts: discountExamples});
co.scan('VOUCHER').scan('VOUCHER').scan('TSHIRT');
co.total();

//Additional examples
const examples = {
  1: ['VOUCHER', 'TSHIRT', 'MUG'],
  2: ['VOUCHER', 'TSHIRT', 'VOUCHER'],
  3: ['TSHIRT', 'TSHIRT', 'TSHIRT', 'VOUCHER', 'TSHIRT'],
  4: ['VOUCHER', 'TSHIRT', 'VOUCHER', 'VOUCHER', 'MUG', 'TSHIRT', 'TSHIRT']
};

for (let example in examples) {
  let coExample = new Checkout({products, discounts: discountExamples});
  coExample.multiScan(examples[example]);
  coExample.total();
}
