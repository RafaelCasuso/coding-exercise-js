# Cabify JavaScript Exercise
ES6 implementation with both NodeJS and Browser substantiation of Cabify JavaScript Challenge as described [here](https://gist.github.com/patriciagao/7c0e87279e8a11a6d85f). 

It is a **Checkout** process that allows scanning multiple products in any order from a given list into a cart, applying some configurable discounts from a given list and getting a total result.


## Installation
To check this project, you will need to clone this repository.

```
git clone https://RafaelCasuso@bitbucket.org/RafaelCasuso/coding-exercise-js.git
```

And install its dependencies with NodeJS package manager.

```
npm install
```

That's it. You have it ready for use.

## Basic Usage

### NodeJS
You can check the original exercise requirements and all example executions, just typing:

```
npm start
```

### Browser
In order to use this code on Browser you need to build it into a minified, transpiled, ready-to-be-bundled file with
Babel just typing:
```
npm run build
```
This will generate such file into */build/checkout.js* so it can be bundled with your favourite bundler (Webpack, for instance).

## API
This code implements a **Checkout** class that can receive an arbitrary set of products and discounts' configuration to
 process a typical cart of items built scanning each one.
### Configuration
**Checkout** must be configured with *Pricing Rules* as declared in the exercise description, so it can be used like this:
```
let co = new Checkout(pricingRules)
```
*Pricing Rules* include:
- **products** (required): A dictionary with `CODE` as index and *name* and `price` as values.
- **discounts** (optional): A dictionary with `DISCOUNT` existing code as index and configuration variables as values.
- **currency** (optional): A string with a valid currency. 

So you can declare each separately (typically they would be stored in database or json). Example:
```
const pricingRules = {
    products: {
      'VOUCHER': {
          name: 'Cabify Voucher',
          price: 5.00
        },
      'TSHIRT': {
          name: 'Cabify T-Shirt',
          price: 20.00
      },
      'MUG': {
          name: 'Cabify Coffee Mug',
          price: 7.50
      }
    }
}
```
> If not products are provided, Checkout initialization will throw an error.

Optionally you can pass **discounts** like this:
```
const pricingRules = {
    products: {
      ...
    },
    discounts: {
      'N_FOR_N': {
          initialNumber: 2,
          finalNumber: 1,
          itemType: 'VOUCHER'
        },
      'BULK_PURCHASE': {
          minNumber: 3,
          itemPrice: 19,
          itemType: 'TSHIRT'
      }
    }
}
```
> Discount code existence is checked at initialization and will throw an error if trying to use an
unregistered one.

And optionally you could pass the **currency** as follows:
```
const pricingRules = {
    products: {
      ...
    },
    discounts: {
      ...
    },
    currency: 'euro'
}
```

> If not passed, default currency will be **Euro**. Allowed currencies would be typically stored at a database or JSON.
 If you try to pass a non-defined currency, it will throw an error.

### Scan product
Once **Checkout** is configured, it is ready to scan products. It can be used in a chaining way like this: 

```
co.scan('VOUCHER').scan('VOUCHER').scan('TSHIRT');
```
> The `scan` method checks safely if the Product Type exists in the store. Invalid Product Types will throw an error.
### Scan multiple products
You can even scan multiple products in bulk, using `multiScan` method as shown below:
```
co.multiScan(['VOUCHER', 'TSHIRT', 'VOUCHER', 'VOUCHER', 'MUG', 'TSHIRT', 'TSHIRT']);
```
> The `multiScan` method is used when processing some big examples in index.js
### Total
The `total` method returns the calculated price of all scanned items with suitable currency symbol.
```
co.total() // 74.50€
```
## Tests
This code reaches **100% Testing Coverage** on both Functions and Lines. BDD-style tests can be run with:
```
npm test
```
### Live testing
Further development can be done using live testing reloading through Mocha --watch and can be used just typing:
```
npm run test:watch
```
### Coverage
Testing coverage HTML report can be generated through:
```
npm run test:coverage
```
And then consulted in */test/coverage/index.html*
### Type Static Analysis
An interesting feature included in this code is annotated **Flow Type** implementation in development mode (it is
 completely stripped in transpiling) to perform **Type Checking Static Analysis**. Thus, all objects, variables,
  constructors, methods and functions described are statically analysed in development for safe type usage, avoiding
  common further errors in runtime.

Type checking can be performed anytime with:
```
npm run test:types
```
## Directory Structure and Naming
This code was built in the strong believe of BDD *semantic naming* for files, classes, functions and directories.
Modularity is in its core for both code source and tests. `Store` directory includes modules for objects that would be
typically stored in database or JSON. `Business` directory includes modules for *Business Logic* objects.
## Style Guide
This code was developed under AirBnB JavaScript Style Guide.

> This code is compliant with JSHint rules as defined in AirBnB JavaScript Style Guide, but used .jshintrc
was not finally included because JSHint is not compatible with FlowType right now.

## Author
Rafael Casuso (@Rafael_Casuso)
## Licence
MIT