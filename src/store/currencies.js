'use strict';

export type currency = {
  name: string,
  symbol: string
};

export const currencies = {
  'euro': {
    name: 'euro',
    symbol: '€'
  }
};