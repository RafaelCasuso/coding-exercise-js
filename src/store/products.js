'use strict';

export type product = {
  [a:string]:{
    name: string,
    price: number
  }
}

export type items = {
  [item: string]: {
    quantity: number,
    price: number
  }
};

export const products =  {
  'VOUCHER': {
    name: 'Cabify Voucher',
    price: 5.00
  },
  'TSHIRT': {
    name: 'Cabify T-Shirt',
    price: 20.00
  },
  'MUG': {
    name: 'Cabify Coffee Mug',
    price: 7.50
  }
};