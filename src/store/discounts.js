'use strict';

export type discountTypes = {
  [a:string]:{
    initialNumber: number,
    finalNumber: number,
    itemType: string
  },
  [b:string]:{
    minNumber: number,
    itemPrice: number,
    itemType: string
  }
}

export const discountExamples = {
  'N_FOR_N': {
    initialNumber: 2,
    finalNumber: 1,
    itemType: 'VOUCHER'
  },
  'BULK_PURCHASE': {
    minNumber: 3,
    itemPrice: 19,
    itemType: 'TSHIRT'
  }
};