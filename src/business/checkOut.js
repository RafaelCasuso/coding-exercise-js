'use strict';

// @flow

import type {product} from '../store/products';
import type {discountTypes} from '../store/discounts';
import {discounts} from './discounts';
import Cart from './cart';

export default class Checkout {

  cart: Cart;
  _discounts: discountTypes;

  constructor(pricingRules: {products: product, discounts: discountTypes, currency?: string}) {
    this._discounts = null;
    if (pricingRules) {
      if (pricingRules.products){
        this.cart = new Cart(pricingRules.products, pricingRules.currency);
      } else {
        throw new Error('Products must be provided to initialize Checkout');
      }
      if (pricingRules.discounts) {
        Object.keys(pricingRules.discounts).forEach((rule) => {
          if (Object.keys(discounts).indexOf(rule) === -1) {
            throw new Error(`Discount doesn't exist: ${rule}`);
          }
        });
        this._discounts = pricingRules.discounts;
      }
    }
  }

  scan(itemType: string) {
    this.cart.addItem(itemType);
    return this;
  }

  multiScan(itemTypes: string[]) {
    itemTypes.forEach((itemType) => this.scan(itemType));
  }

  total() {
    let total = 0;
    if (this._discounts) {
      for (let rule in this._discounts) {
        discounts[rule](this.cart.items, this._discounts[rule]);
      }
    }
    for (let item in this.cart.items) {
      total += this.cart.items[item].quantity * this.cart.items[item].price;
    }
    let itemsDescription = Object.keys(this.cart.items).map((item) =>
      item = ` ${item} x ${this.cart.items[item].quantity}`);
    console.log(`Items: ${itemsDescription.toString()}`);
    console.log(`Total: ${total.toFixed(2)}${this.cart._currency.symbol}`);
    return `${total.toFixed(2)}${this.cart._currency.symbol}`;
  }
}
