'use strict';

// @flow

import type {items} from '../store/products';

export const discounts = {
  'N_FOR_N' (items: items, options: {
    initialNumber: number,
    finalNumber: number,
    itemType: string
  }) {
    if (items[options.itemType].quantity >= options.initialNumber) {
      items[options.itemType].price = ((Math.floor(items[options.itemType].quantity /
          options.initialNumber) * options.finalNumber + (items[options.itemType].quantity %
        options.initialNumber)) * items[options.itemType].price) /
        items[options.itemType].quantity;
    }
  },
  'BULK_PURCHASE' (items: items, options: {
    minNumber: number,
    itemPrice: number,
    itemType: string
  }) {
    if (items[options.itemType].quantity >= options.minNumber) {
      items[options.itemType].price = options.itemPrice;
    }
  }
};