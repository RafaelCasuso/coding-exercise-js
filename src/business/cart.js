'use strict';

// @flow

import {currencies} from '../store/currencies';
import type {product} from '../store/products';
import type {items} from '../store/products';
import type {currency} from '../store/currencies';

export default class Cart {

  items: items;
  _currency: currency;
  _products: product;

  constructor(products: product, currency: string = 'euro') {
    this.items = {};
    if (!products){
      throw new Error('You need products to initialize Cart');
    }
    if (currencies[currency]) {
      this._currency = currencies[currency];
    } else {
      throw new Error('Invalid currency');
    }
    this._products = products;
  }

  addItem(itemType: string) {
    if (Object.keys(this._products).indexOf(itemType) !== -1) {
      if (this.items[itemType]) {
        this.items[itemType].quantity++;
      } else {
        this.items[itemType] = {
          quantity: 1,
          price: this._products[itemType].price
        };
      }
    } else {
      throw new Error(`Invalid item type: ${itemType}`);
    }
  }
}
